import React, { useEffect, useState, useRef } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TouchableOpacity,
  Image,TextInput,
  SafeAreaView,
  ScrollView,
} from 'react-native';
const Otp = ({ navigation }) => {
  let textInput = useRef(null);
  const lengthInput = 4;
  const [internalVal, setInternalVal] = useState('');
  
  const onChangeText = (val) => {
    setInternalVal(val);
  };

  const Bold = ({ children }) => (
    <Text style={{ fontWeight: 'bold', color: 'black' }}>{children}</Text>
  );
  const Bold1 = ({ children }) => (
    <Text style={{ fontWeight: 'bold', color: 'red' }}>{children}</Text>
  );
  return (
    <ScrollView>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 12,
            height: 80,
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            style={{ marginLeft: 8 }}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../assets/images/back_arrow_icon.png')}
              resizeMode="contain"
              style={{
                width: 25,
                height: 25,
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.header}>
          <Image
            source={require('../assets/images/otp_verified.png')}
            resizeMode="contain"
            style={{
              borderBottomLeftRadius: 30,
              height: 200,
              width: 200,
            }}
          />
          <Text style={styles.text_footer}>OTP Verification</Text>
        </View>
        <View style={styles.footer}>
          <Text style={styles.in_footer}>
            Enter the OTP sent to <Bold>+91-9629662903</Bold>
          </Text>

          <View
            style={styles.containerInput}
            onPress={() => {
              console.warn('text');
            }}>
            {Array(lengthInput)
              .fill()
              .map((data, index) => (
                <View key={index} style={styles.callView}>
                  <TextInput style={styles.cellText}>
                    {internalVal && internalVal.length > 0
                      ? internalVal[index]
                      : ' '}
                  </TextInput>
                </View>
              ))}
          </View>
          <Text style={styles.on_footer}>
            Dont receive the OTP? <Bold1> RESEND OTP</Bold1>
          </Text>
          <Text> </Text>
          <Text> </Text>

          <TouchableOpacity style={styles.appButtonContainer}>
            <Text
              style={styles.appButtonText}
              secureTextEntry={true}
              color="grey"
              align="center"
              onPress={() =>navigation.navigate('MyDrawer')}>
              Verify & proceed
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};
export default Otp;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FEFEFA',
    justifyContent:'flex-start',
    alignItems:'center'
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 70,
    paddingTop: 1,
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 100,
    borderTopRightRadius: 100,
    paddingHorizontal: 30,
    paddingVertical: 30,
    paddingBottom: 70,
  },
  containerInput: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  callView: {
    paddingVertical: 11,
    width: 40,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1.5,
  },
  cellText: {
    textAlign: 'center',
    fontSize: 16,
  },
  textinput: {
    alignSelf: 'stretch',
    height: 55,
    marginBottom: 30,
    color: 'black',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    paddingBottom: 8,
    borderBottomColor: '#A9A9A9',
    borderBottomWidth: 2,
    maxWidth: 500,
  },
  text_header: {
    color: '#0f73ee',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_footer: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 25,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
    textAlign: 'center',
  },
  in_footer: {
    color: '#A9A9A9',
    fontWeight: 'normal',
    fontSize: 15,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
  },
  on_footer: {
    color: '#A9A9A9',
    fontWeight: 'normal',
    fontSize: 17,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  appButtonContainer: {
    elevation: 6,
    backgroundColor: '#0f73ee',
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 15,
    width: 300,
    height: 50,
  },
  appButtonText: {
    fontSize: 25,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    textTransform: 'uppercase',
  },
});