
import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Button,
  Alert,
  ScrollView, AsyncStorage, Picker
} from 'react-native';
import { RadioButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { addUser, getAllData } from '../services/Apiservices'
import DropDownPicker from 'react-native-dropdown-picker';
import DatePicker from 'react-native-datepicker';
import ActivityLoading from '../components/ActivityLoading'

class Updateprofile extends React.Component {
  state = {
    isLoading: false,
    userName: ' ',
    password: ' ',
    confirmpassword: ' ',
    active: ' ',
    firstName: ' ',
    lastName: ' ',
    dob: ' ',
    accessLocation: ' ',
    jwt_token: '',
    user_id: '',
    locationArray: [],
    date: new Date(),


    fName: '',
    lName: '',
    mName: '',
    gender: 'Male',
    phoneNumber: '',
    altPhone: '',
    email: '',
    altEmail: '',
    address1: '',
    address2: '',
    city: '',
    district: '',
    area: '',
    state: '',
    pinCode: '',
    education: '',
    completionYear: '',
    collegeName: '',
    collegeCity: '',
    collegeState: '',
    interestedCareer: [],
    designation: '',
    companyName: '',
    companyDuration: '',
    companyCity: '',
    companyState: ''



  };
  onValidate() {
    { this.state.password == this.state.confirmpassword ? this.doLogin() : console.log("Conform password is not Matched") };
  };
  onChangeHandle(state, value) {
    this.setState({
      [state]: value,
    });
  }
  componentDidMount() {

    AsyncStorage.getItem('userToken').then(async (res) => {
      const jwt = await res;

      getAllData('get_location/V1.0', jwt)
        .then((response) => {

          if (response.statusMessage.code == 200) {

            this.copyArray(response.data)
            this.setState({
              state: response.data[0].stateName,
              city: response.data[0].city[0].cityName
            });

          } else {
          }
        })
        .catch((error) => {
        })
      this.setState({
        jwt_token: jwt,
      });
      AsyncStorage.getItem('userId').then(async (res) => {
        const userId = await res;
        this.setState({
          user_id: userId,
        });

      });
    });
  }

  copyArray = (data) => {
    console.warn("get data", JSON.stringify(data));

    const output = []
    data.map(function (x) {

      output.push(...x.city);
      console.log('city', output);

    })
    console.log('city', output);
    const booth_ = []
    output.map(function (y) {
      booth_.push(...y.booth);
    })
    console.log('city', booth_);
    this.setState({
      locationArray: booth_
    });
  }

  doLogin() {
    const {
      userName,
      password,
      confirmpassword,
      active,
      firstName,
      lastName,
      dob,
      gender,
      accessLocation,
    } = this.state;

    this.setState({
      isLoading: true
    });


    let user_profile = {

      firstName: this.state.fName,
      lastName: this.state.lName,
      fatherName: this.state.lName,
      dob: this.state.date,
      gender: this.state.gender,
      email: this.state.email,
      phoneNumber: this.state.phoneNumber,
      alternateMobile: this.state.altPhone,
      address1: this.state.address1,
      address2: this.state.address2,
      town: this.state.city,
      district: this.state.district,
      state: this.state.state,
      pincode: this.state.pinCode,
      employmentStatus: "Employed",
      education: {
        domain: this.state.education,
        name: this.state.collegeName,
        completed: this.state.completionYear,
        city: this.state.collegeCity,
        state: this.state.collegeState
      },
      profession: {
        designation: this.state.designation,
        name: this.state.companyName,
        joining: this.state.companyDuration,
        city: this.state.companyCity,
        state: this.state.companyState
      },
      intrestCarrier: this.state.interestedCareer

    }

    addUser('update_admin/' + this.state.user_id + '/V1.0', user_profile, this.state.jwt_token)
      .then((response) => {
        if (response.status == 200) {
          this.props.navigation.goBack()
          this.setState({
            isLoading: false
          });
        }
        else if (response.status == 500) {
          this.setState({
            isLoading: false
          });
          Alert.alert("Your Profile was not updated. Please try again");
        }

      })
      .catch((error) => {
        this.setState({
          isLoading: false
        });
        Alert.alert("Something went wrong, Try again");

      });
  }


  render() {
    const {
      userName,
      password,
      confirmpassword,
      active,
      firstName,
      lastName,
      dob,
      gender,
      accessLocation, locationArray, isLoading
    } = this.state;

    let boothArray = locationArray.map((s, i) => {
      return <Picker.Item key={i} value={s.boothName} label={s.boothName} />
    });
    if (isLoading) {
      return (
        <View style={styles.regform}>
          <ActivityLoading size="large" />
        </View>
      );
    }
    else {
      return (
        <ScrollView style={styles.scrollView}>


          <View style={styles.regform}>

            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>First Name </Text>
              <TextInput style={styles.textinput}
                placeholder="Enter your First Name" underlineColorAndroid={'transparent'}
                onChangeText={(text) =>
                  this.setState({
                    fName: text,
                  })}

              />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Second Name  </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) => this.setState({
                  lName: text,
                })
                }
                placeholder="Enter your Second Name" underlineColorAndroid={'transparent'} />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Father Name </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) =>
                  this.setState({
                    mName: text,
                  })}
                placeholder="Enter your Father's Name" underlineColorAndroid={'transparent'} />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Date of Birth </Text>
              <DatePicker
                style={{ width: 250 }}
                date={this.state.date}
                mode="date"
                placeholder="select date"
                format="DD-MM-YYYY"
                minDate="01-01-1990"
                maxDate="01-01-2021"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 36
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(date) => this.setState({
                  date: text,
                })}
              />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Gender  </Text>
              <RadioButton.Group onValueChange={value => this.setState({
                gender: value,
              })} value={this.state.gender}>
                <RadioButton.Item label="Male" value="Male" />
                <RadioButton.Item label="Female" value="Female" />
                <RadioButton.Item label="Others" value="Others" />
              </RadioButton.Group>
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Phone Number </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) => this.setState({
                  phoneNumber: text,
                })}
                keyboardType='numeric'
                maxLength={10}
                placeholder="Enter your Phone Number" underlineColorAndroid={'transparent'} />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Alternate Number </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) => this.setState({
                  altPhone: text,
                })}
                keyboardType='numeric'
                maxLength={10}
                placeholder="Enter your Alternate Number" underlineColorAndroid={'transparent'} />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Email </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) => this.setState({
                  email: text,
                })}

                placeholder="Enter your Email" underlineColorAndroid={'transparent'} />
            </View>


            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Address - 1 </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) => this.setState({
                  address1: text,
                })}
                placeholder="Enter your address 1" underlineColorAndroid={'transparent'} />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Address - 2 </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) => this.setState({
                  address2: text,
                })}
                placeholder="Enter your address 2" underlineColorAndroid={'transparent'} />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Town/City  </Text>
              <TextInput style={styles.textinput}
                value={this.state.city}
                onChangeText={(text) => this.setState({
                  city: text,
                }
                )}
                placeholder="Village / Town" underlineColorAndroid={'transparent'} />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>District </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) => this.setState({
                  district: text,
                }
                )}
                placeholder="Enter your district" underlineColorAndroid={'transparent'} />
            </View>

            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>State </Text>
              <TextInput style={styles.textinput}
                value={this.state.state}

                onChangeText={(text) => this.setState({
                  state: text,
                })}
                placeholder="Enter your State" underlineColorAndroid={'transparent'} />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Pin code </Text>
              <TextInput style={styles.textinput}
                onChangeText={(text) => this.setState({
                  pinCode: text,
                }
                )}
                keyboardType='numeric'
                maxLength={6}
                placeholder="Enter your pin code" underlineColorAndroid={'transparent'} />
            </View>



            <View style={styles.textInputContainer}>
              <View style={styles.textinput}>
                <Text style={styles.Label}>Area/Location </Text>
                <Picker style={styles.Picker}
                  selectedValue={this.state.area}
                  onValueChange={(text) => {
                    this.setState({
                      area: text,
                    }
                    )
                  }

                  }>
                  {
                    boothArray != '' ?
                      <Picker.Item label='Select Area/location' value="" />
                      :
                      <Picker.Item label='xyz' value="xyz" />
                  }
                  {
                    boothArray
                  }
                </Picker>
              </View>
            </View>




            <View style={styles.subCard}>
              <Text
                style={styles.subTitle}
                secureTextEntry={true}>
                Add Education Details
             </Text>
              <View>
                <Text style={styles.textcolor}>Education Qualification</Text>
                <DropDownPicker
                  items={[
                    { label: 'PUC', value: 'puc' },
                    { label: 'ITI', value: 'iti' },
                    { label: 'Diploma', value: 'dimploma' },
                    { label: 'BA', value: 'ba' },
                    { label: 'BSC', value: 'bsc' },
                    { label: 'BCom', value: 'bcom' },
                    { label: 'BBM', value: 'bbm' },
                    { label: 'BCA', value: 'bca' },
                    { label: 'BBA', value: 'bba' },
                    { label: 'Btech', value: 'btech ' },
                    { label: 'BE', value: 'be' },
                    { label: 'MSC', value: 'msc' },
                    { label: 'MCom', value: 'mcom' },
                    { label: 'MBA', value: 'mba' },
                    { label: 'ME', value: 'me' },
                    { label: 'Mtech', value: 'mtech' },
                  ]}
                  defaultNull
                  placeholder="Select Your Qualification"
                  containerStyle={{ height: 40, padding: 5, marginTop: 5 }}
                  onChangeItem={(item) => {
                    this.setState({
                      education: item.value,
                    }
                    )
                  }}
                />
              </View>
              <View>
                <Text style={styles.textcolor}>School/College Name</Text>
                <TextInput style={styles.textinput} placeholder="Enter School/College Name" underlineColorAndroid={'transparent'}
                  onChangeText={(text) =>
                    this.setState({
                      collegeName: text
                    })
                  } />
              </View>

              <View>
                <Text style={styles.textcolor}>Completion Year</Text>
                <DropDownPicker
                  items={[
                    { label: '2000', value: '2000' },
                    { label: '2001', value: '2001' },
                    { label: '2002', value: '2002' },
                    { label: '2003', value: '2003' },
                    { label: '2004', value: '2004' },
                    { label: '2005', value: '2005' },
                    { label: '2006', value: '2006' },
                    { label: '2007', value: '2007' },
                    { label: '2008', value: '2008' },
                    { label: '2009', value: '2009' },
                    { label: '2010', value: '2010' },
                    { label: '2011', value: '2011' },
                    { label: '2012', value: '2012' },
                    { label: '2013', value: '2013' },
                    { label: '2014', value: '2014' },
                    { label: '2015', value: '2015' },
                    { label: '2016', value: '2016' },
                    { label: '2017', value: '2017' },
                    { label: '2018', value: '2018' },
                    { label: '2019', value: '2019 ' },
                    { label: '2020', value: '2020' }

                  ]}
                  defaultNull
                  placeholder="Completion year"
                  containerStyle={{ height: 40, padding: 5, marginTop: 5 }}
                  onChangeItem={(item) => {
                    this.setState({
                      completionYear: item.value,
                    }
                    )
                  }}
                />
              </View>
              <View>
                <Text style={styles.textcolor}>City</Text>
                <TextInput style={styles.textinput} placeholder="Year of Completion" underlineColorAndroid={'transparent'}

                  onChangeText={(text) => this.setState({
                    collegeCity: text
                  })} />
              </View>
              <View>
                <Text style={styles.textcolor}>State</Text>
                <TextInput style={styles.textinput} placeholder="Year of Completion" underlineColorAndroid={'transparent'}

                  onChangeText={(text) => this.setState({
                    collegeState: text
                  })} />
              </View>

            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.textcolor}>Interested Carrier <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
              <View>
                <DropDownPicker
                  items={[
                    { label: 'Engineer', value: 'Engineer' },
                    { label: 'Government jobs', value: 'Government jobs' },
                    { label: 'Private Business', value: 'Private Business' },
                    { label: 'Doctor', value: 'Doctor' },
                    { label: 'Teacher', value: 'Teacher' },
                    { label: 'Sports', value: 'Sports' },
                  ]}

                  multiple={true}
                  defaultValue={this.state.interestedCareer}
                  containerStyle={{ height: 40 }}
                  onChangeItem={(item) => {
                    this.setState({
                      interestedCareer: item
                    })
                  }
                  }
                />
              </View>

            </View>
            <View>
              <Text
                style={styles.subTitle}
                secureTextEntry={true}>
                Add Employment Details
             </Text>
              <View style={styles.textInputContainer}>
                <Text style={styles.textcolor}>Designation </Text>
                <TextInput style={styles.textinput}
                  onChangeText={(text) => this.setState({
                    designation: text,
                  }
                  )}
                  placeholder="Enter your designation" underlineColorAndroid={'transparent'} />
              </View>

              <View style={styles.textInputContainer}>
                <Text style={styles.textcolor}>Company Name </Text>
                <TextInput style={styles.textinput}
                  onChangeText={(text) => this.setState({
                    companyName: text,
                  }
                  )}
                  placeholder="Enter your Company Name" underlineColorAndroid={'transparent'} />
              </View>
              <View style={styles.textInputContainer}>
                <Text style={styles.textcolor}>Joining Year </Text>
                <TextInput style={styles.textinput}
                  keyboardType='numeric'
                  maxLength={4}
                  onChangeText={(text) => this.setState({
                    companyDuration: text,
                  }
                  )}
                  placeholder="Enter your Joining year" underlineColorAndroid={'transparent'} />
              </View>
              <View style={styles.textInputContainer}>
                <Text style={styles.textcolor}>City </Text>
                <TextInput style={styles.textinput}
                  onChangeText={(text) => this.setState({
                    companyCity: text,
                  }
                  )}
                  placeholder="Enter City" underlineColorAndroid={'transparent'} />
              </View>
              <View style={styles.textInputContainer}>
                <Text style={styles.textcolor}>State </Text>
                <TextInput style={styles.textinput}
                  onChangeText={(text) => this.setState({
                    companyState: text,
                  }
                  )}
                  placeholder="Enter State" underlineColorAndroid={'transparent'} />
              </View>
            </View>

            <View >
              <TouchableOpacity style={styles.appButtonContainer}>
                <Text
                  style={styles.appButtonText}
                  secureTextEntry={true}
                  color="grey"
                  align="center"
                  onPress={() => this.doLogin()}>
                  Submit
           </Text>
              </TouchableOpacity>
            </View>

          </View>
        </ScrollView>
      );
    }
  }
}
export default Updateprofile;
const styles = StyleSheet.create({
  regform: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    alignSelf: 'stretch',
  },
  header: {
    fontSize: 24,
    color: '#0f73ee',
    paddingBottom: 5,
    marginBottom: 20,
    marginTop: 20,
    paddingLeft: 65,
  },
  textinput: {
    borderWidth: 1,
    fontSize: 14,
    color: 'black',
    borderColor: '#A9A9A9',
  },
  scrollView: {
    paddingLeft: 10,
    marginHorizontal: 5,
    paddingRight: 10,
  },
  appButtonContainer: {
    elevation: 6,
    backgroundColor: '#0f73ee',
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginBottom: 20,
  },
  appButtonText: {
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingRight: 30,
    paddingLeft: 20
  },
  textcolor: {
    color: '#808080',
  },
  textInputContainer: {
    padding: 3,
    justifyContent: 'flex-start'
  },
  subCard: { flex: 1, marginTop: 5 }
});