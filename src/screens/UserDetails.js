// import React from 'react';
// import {
//   View,
//   Text,
//   ImageBackground,
//   TouchableOpacity,
//   Image,
//   ScrollView,
//   Animated,
//   Alert,
//   StyleSheet, AsyncStorage
// } from 'react-native';
// import { delUser } from '../services/Apiservices'
// import RoundedButton from '../components/RoundedButton';
// const ListUserDetail = ({ route, navigation }) => {
//   const [book, setBook] = React.useState(' ');
//   const [jwt_token, setJwt] = React.useState(' ');

//   React.useEffect(() => {
//     let { book } = route.params;
//     setBook(book);

//     AsyncStorage.getItem('userToken').then(async (res) => {
//       const jwt = await res;
//       // console.warn("get async", t);
//       setJwt(jwt);

//     });

//   }, [book]);
//   console.log(book);


//   const delete_user = () => {
//     console.log(book.userId);

//     delUser('delete_user/' + book.userId + '/V1.0', jwt_token)
//       .then((response) => {

//         if (response.status == 200) {
//           Alert.alert("User Deleted Successfully");
//           navigation.goBack();
//         }

//       })
//       .catch((error) => {

//         Alert.alert("Something went wrong, Try again");

//       });

//   }

//   return (
//     <View>

//       <View
//         style={{
//           flexDirection: 'column',
//           paddingHorizontal: 12,
//           height: 50,
//           alignItems: 'center',
//         }}>
//         <Text style={styles.text}>
//           <Image
//             style={{ height: 20, width: 20 }}
//             source={{
//               uri:
//                 'https://research.cbc.osu.edu/sokolov.8/wp-content/uploads/2017/12/profile-icon-png-898.png',
//             }}
//           />{' '}
//           {book.firstName}
//           <Text> </Text>
//         </Text>
//         <Text style={styles.text2}>
//           <Image
//             style={{ height: 20, width: 20 }}
//             source={{
//               uri: 'https://www.iconsdb.com/icons/preview/gray/email-2-xxl.png',
//             }}
//           />{' '}
//           {book.email}{' '}
//         </Text>
//         <Text style={styles.text2}>
//           <Image
//             style={{ height: 20, width: 20 }}
//             source={{
//               uri:
//                 'https://cdn3.iconfinder.com/data/icons/maps-and-navigation-colored-1/48/37-512.png',
//             }}
//           />{' '}
//           {book.city} <Text> </Text>
//           <Image
//             style={{ height: 20, width: 20 }}
//             source={{
//               uri:
//                 'https://cdn2.iconfinder.com/data/icons/gaming-and-beyond-part-2-1/80/Phone_gray-512.png',
//             }}
//           />
//           <Text> </Text>
//           {book.phoneNumber}
//         </Text>

//         <View >
//           <RoundedButton 
//           label={  "Delete User"}
//           onPress={() => delete_user()}
//           bgColor ={'red'} 
//           style={{margin:5,padding:5,paddingLeft:10,paddingRight:10,borderRadius:5}}>

//           </RoundedButton>
//         </View>

//       </View>


//     </View>
//   );
// };
// export default ListUserDetail;
// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: '#F5FCFF',
//     alignContent: 'center',
//     alignItems: 'center',
//   },
//   text: {
//     marginLeft: 12,
//     fontSize: 20,
//   },
//   text2: {
//     marginLeft: 12,
//     fontSize: 16,
//     color: 'gray',
//   },
// });
import React from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  ScrollView,
  Animated,
  Alert,
  StyleSheet,
  TextInput,
} from 'react-native';
import { delUser } from '../services/Apiservices'
import CommonStyles from '../constants/styles';
import RoundedButton from '../components/RoundedButton';

import Moment from 'moment';

const ListUserDetail = ({ route, navigation }) => {
  const [book, setBook] = React.useState(' ');
  const [jwt_token, setJwt] = React.useState(' ');

  React.useEffect(() => {
    let { book } = route.params;
    setBook(book);
    AsyncStorage.getItem('userToken').then(async (res) => {
      const jwt = await res;
      setJwt(jwt);

    });
  }, [book]);
  Moment.locale('en');

  const delete_user = () => {

    delUser('delete_user/' + book.userId + '/V1.0', jwt_token)
      .then((response) => {

        if (response.status == 200) {
          Alert.alert("User Deleted Successfully");
          navigation.goBack();
        }

      })
      .catch((error) => {

        Alert.alert("Something went wrong, Try again");

      });

  }

  return (
    <ScrollView>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 12,
            alignItems: 'flex-end',
          }}>
        </View>
        <View style={styles.header}>
          <View>
            <Text>Name:</Text>
            <Text style={styles.text_header}>
              <Image
                style={{ height: 30, width: 30 }}
                source={{
                  uri:
                    'https://research.cbc.osu.edu/sokolov.8/wp-content/uploads/2017/12/profile-icon-png-898.png',
                }}
              />
              {book.firstName}
            </Text>
          </View>
          <View>
            <Text>Phone:</Text>
            <Text style={styles.text_header}>
              <Image
                style={{ height: 20, width: 20 }}
                source={{
                  uri:
                    'https://www.iconsdb.com/icons/preview/gray/email-2-xxl.png',
                }}
              />

              {book.email}

            </Text>
          </View>
          <View>
            <Text>City/Town:</Text>

            <Text style={styles.text_header}>
              <Image
                style={{ height: 20, width: 20 }}
                source={{
                  uri:
                    'https://cdn3.iconfinder.com/data/icons/maps-and-navigation-colored-1/48/37-512.png',
                }}
              />

              {book.city}

            </Text>
          </View>
          <View>
            <Text>Phone:</Text>
            <Text style={styles.text_header}>
              <Image
                style={{ height: 20, width: 20 }}
                source={{
                  uri:
                    'https://cdn2.iconfinder.com/data/icons/gaming-and-beyond-part-2-1/80/Phone_gray-512.png',
                }}
              />

              {book.phoneNumber}
              <Text>

                {book.alternatePhone}
              </Text>

            </Text>
          </View>
          <View>
            <Text>Gender:</Text>
            <Text style={styles.text_header}>
              <Image
                style={{ height: 20, width: 20 }}
                source={{
                  uri:
                    'https://cdn2.iconfinder.com/data/icons/users-20/24/gender-female-male-512.png',
                }}
              />

              {book.gender}
            </Text>
          </View>
          <View style={styles.textViewContainer}>
            <Text>Age:</Text>
            <Text style={styles.text_header}>
              <Image
                style={{ height: 20, width: 20 }}
                source={{
                  uri:
                    'https://cdn4.iconfinder.com/data/icons/communication-extras/512/Birthday_Cake-512.png',
                }}
              />
              {Moment().diff(book.birthDate, 'years', false)} years
            {/* {Moment(book.birthDate).fromNow()} */}
            </Text>
          </View>

          <View>
            <RoundedButton
              label={"Delete User"}
              onPress={() => delete_user()}
              bgColor={'red'}
              style={{ margin: 5, padding: 5, paddingLeft: 10, paddingRight: 10, borderRadius: 5 }}>

            </RoundedButton>
          </View>


        </View>
      </View>
    </ScrollView>
  );
};
export default ListUserDetail;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: CommonStyles.color.COLOR_BACKGROUND_PRIMARY,
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 10,
    paddingTop: 1,
    backgroundColor: '#FFFF',
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 100,
    borderTopRightRadius: 100,
    paddingHorizontal: 30,
    paddingVertical: 30,
    paddingBottom: 70,
    justifyContent: 'flex-end',
  },
  textinput: {
    alignSelf: 'stretch',
    height: 55,
    color: 'black',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    borderBottomColor: '#A9A9A9',
    borderBottomWidth: 2,
  },
  textViewContainer:{
    padding:5,
    marginTop:5
  },
  text_header: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
  },
  text2: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  errorMsg: {
    color: '#b20000',
    fontSize: 14,
    textAlign: 'center',
  },
  text_footer: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 25,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
    textAlign: 'center',
    justifyContent: 'flex-end',
  },
  in_footer: {
    color: '#A9A9A9',
    fontWeight: 'normal',
    fontSize: 15,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
  },
  on_footer: {
    color: '#A9A9A9',
    fontWeight: 'normal',
    fontSize: 17,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  appButtonContainer: {
    elevation: 6,
    backgroundColor: '#0f73ee',
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 15,
    width: 300,
    height: 50,
  },
  appButtonText: {
    fontSize: 25,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    textTransform: 'uppercase',
  },
});